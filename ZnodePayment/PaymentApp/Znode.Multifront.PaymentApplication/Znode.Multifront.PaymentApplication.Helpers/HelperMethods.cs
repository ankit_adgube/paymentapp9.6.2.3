﻿using System;
using System.Collections.Specialized;

namespace Znode.Multifront.PaymentApplication.Helpers
{
    public class HelperMethods
    {
        /// <summary>
        /// Format Credit Card Number According to CreditCardFormat Key in Webconfig
        /// </summary>
        /// <param name="CreditCardLastFourDigit">string CreditCardLastFourDigit</param>
        /// <returns>Formated Credit Card Number</returns>
        public static string FormatCreditCardNumber(string CreditCardLastFourDigit)
            => $"{Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CreditCardFormat"])}{CreditCardLastFourDigit}";

        /// <summary>
        /// Set paging parameters 
        /// </summary>
        /// <param name="page">page</param>
        /// <param name="pagingStart">pagingStart</param>
        /// <param name="pagingLength">pagingLength</param>
        public static void SetPaging(NameValueCollection page, out int pagingStart, out int pagingLength)
        {
            // We use int.MaxValue for the paging length to ensure we always get total results back
            pagingStart = 1;
            pagingLength = int.MaxValue;
            if (!Equals(page, null) && page.HasKeys())
            {
                // Only do if both index and size are given
                if (!string.IsNullOrEmpty(page.Get(PageKeys.Index)) && !string.IsNullOrEmpty(page.Get(PageKeys.Size)))
                {
                    pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
                    pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
                }
            }
        }
    }
}
